import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../Cinema/ListCinema.dart';
import '../homescreen.dart';
import '../Proflie/profilescreen.dart';
import '../SliverScreen/SliverScreen.dart';

class CinemaScreen extends StatefulWidget {
  const CinemaScreen({Key? key}) : super(key: key);
  @override
  _CinemaScreenState createState() => _CinemaScreenState();
}

class _CinemaScreenState extends State<CinemaScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Align(
              child: Text("RẠP PHIM"),
              alignment: Alignment.center,
            ),
            backgroundColor: Color(0xF2D59CDC)
        ),
        body:
            Home(),
      bottomNavigationBar: Container(
        height: 65,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color:  Color(0xF2D59CDC),
                blurRadius: 10
            )
          ],
        ),
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const HomeScreen()),
                        );
                      },
                      icon: Icon(Icons.home_outlined)),
                  Text('Trang Chủ')
                ],
              ),
              Column(
                children: [

                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const CinemaScreen()),
                        );
                      },
                      icon: Icon(Icons.camera_roll_outlined)),
                  Text('Rạp Phim')
                ],
              ),
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const SliverScreen()),
                        );
                      },
                      icon: Icon(Icons.camera_alt_outlined)),
                  Text('Điện Ảnh')
                ],
              ),
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const ProfileScreen()),
                        );
                      },
                      icon: Icon(Icons.person_outlined)),
                  Text('Tài Khoản')
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget Home() {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Location(),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 500,
            child: Cinema(),
          )
        ],
      ),
    );
  }
  Widget Location(){
    return Container(
        decoration: BoxDecoration(

        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                Icon(Icons.place_outlined,color:Color(0xF2D59CDC),size: 20,),
              ],
            ),
            Column(
              children: [
                Text('Toàn Quốc',textAlign: TextAlign.center,
                style: TextStyle(color:  Color(0xF2D59CDC),),)
              ],
            ),
          ],

        )
    );
  }


}