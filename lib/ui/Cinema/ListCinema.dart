import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Cinema extends StatelessWidget {
  final cinemaList = [
    {
      'name': 'Galaxy Nguyễn Du',
      'image': 'lib/img/cinema1.jpg',
      'address': '116 Nguyễn Du, Quận 1, Tp.HCM',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Tân Bình',
      'image': 'lib/img/cinema2.jpg',
      'address': '246 Nguyễn Hồng Đào, Quận Tân Bình, Tp.HCM',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Kinh Dương Vương',
      'image': 'lib/img/cinema3.jpg',
      'address': '718bis Kinh Dương Vương, Quận 6, Tp.HCM',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Quang Trung',
      'image': 'lib/img/cinema4.jpg',
      'address': 'Lầu 3, TTTM CoopMart Foodcosa số 304A Quang Trung, Quận Gò Vấp, Tp.HCM',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Bến Tre',
      'image': 'lib/img/cinema5.jpg',
      'address': 'Lầu 1, TTTM Sense City 26A Trần Quốc Toản, Phường 4, Bến Tre',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Mipec Long Biên',
      'image': 'lib/img/cinema6.jpg',
      'address': 'Tầng 6, TTTM Mipec Riverside, Số 2, Phố Long Biên 2, Phường Ngọc Lâm, Quận Long Biên',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Đà Nẵng',
      'image': 'lib/img/cinema7.jpg',
      'address': 'Tầng 3, TTTM Coop Mart, 478 Điện Biên Phủ, Quận Thanh Khê, Đà Nẵng',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Cà Mau',
      'image': 'lib/img/cinema8.jpg',
      'address': 'lầu 2 TTTM Sense City, số 9, Trần Hưng Đạo, Phường 5,Tp.Cà Mau',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Phạm Văn Chí',
      'image': 'lib/img/cinema9.jpg',
      'address': 'Lầu 5, TTTM Platinum Plaza, số 634Bis Đường Phạm Văn Chí, Phường 8, Quận 6,Tp.HCM',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Trung Chánh',
      'image': 'lib/img/cinema10.jpg',
      'address': 'TTVH Quận 12 – Số 09, Quốc Lộ 22, Phường Trung Mỹ Tây , Quận 12,Tp.HCM',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Huỳnh Tấn Phát',
      'image': 'lib/img/cinema11.jpg',
      'address': 'Lầu 2 TTTM Coopmart - 1362 Đường Huỳnh Tấn Phát, Khu phố 1, Phường Phú Mỹ, Quận 7,Tp.HCM',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Vinh',
      'image': 'lib/img/cinema12.jpg',
      'address':'Lầu 5 TTTM Giải Trí City HUB – số 1 Lê Hồng Phong, Tp. Vinh',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Hải Phòng',
      'image': 'lib/img/cinema13.jpg',
      'address': 'Lầu 7, TTTM Nguyễn Kim - Sài Gòn Mall, 104 Lương Khánh Thiện, Lương Khá Thien, Ngô Quyền, Hải Phòng ',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Nguyễn Văn Quá',
      'image': 'lib/img/cinema14.jpg',
      'address':'119B Nguyễn Văn Quá, Đông Hưng Thuận, Quận 12, Tp.HCM',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Tràng Thi',
      'image': 'lib/img/cinema15.jpg',
      'address': 'Lầu 4 TTTM Nguyễn Kim, số 10B Tràng Thi, Hoàn Kiếm, Hà Nội',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Buôn Mê Thuột',
      'image': 'lib/img/cinema16.jpg',
      'address': 'Tầng trệt, TTTM Co.opmart Buôn Ma Thuột, 71 Nguyễn Tất Thành, Tân An, Thành phố Buôn Ma Thuột, Đắk Lắk',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Long Xuyên',
      'image': 'lib/img/cinema17.jpg',
      'address': 'Tầng 1, TTTM Nguyễn Kim, số 01 Trần Hưng Đạo, Phường Mỹ Bình, Tp.Long Xuyên',
      'hotline': '1900 2224'
    },
    {
      'name': 'Galaxy Co.opXtra Linh Trung',
      'image': 'lib/img/cinema18.jpg',
      'address': 'Tầng trệt, TTTM Co.opXtra Linh Trung, số 934 Quốc Lộ 1A, Phường Linh Trung, Quận Thủ Đức,Tp.HCM',
      'hotline': '1900 2224'
    },

  ];


  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        itemCount: cinemaList.length,
        shrinkWrap: true,
        padding: EdgeInsets.all(5),
        scrollDirection: Axis.vertical,
        separatorBuilder: (BuildContext,index)
        {
          return Divider(height: 40);
        },
        itemBuilder: (BuildContext context, int index){
          return CinemaState(
            name: cinemaList[index]['name'],
            image: cinemaList[index]['image'],
            address: cinemaList[index]['address'],
            hotline: cinemaList[index]['hotline'],
          );
        },
    );
  }
}

class CinemaState extends StatelessWidget {
  final name;
  final image;
  final address;
  final hotline;

  CinemaState ({
    this.name,
    this.image,
    this.address,
    this.hotline,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset(image, width: 120,),
      title: Text(name, style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              SizedBox(
                height: 40,
              ),
              Flexible(
                child: Text(
                  address,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.black),
                  ),
              ),
            ],
          ),
          Row(
            children: [
              SizedBox(
                height: 20,
              ),
              Text(hotline,style: TextStyle(color: Colors.black))
            ],
          )
        ],
      ),
    );
  }

}
