import 'package:flutter/material.dart';
import '../ui/FilmShowing/FilmShowing.dart';
import '../ui/UpcomingFilm/UpcomingFilm.dart';

class Category extends StatefulWidget {
  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState  extends State<Category> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 65,
      child: ListView(
      scrollDirection: Axis.horizontal,
      children: [
        SizedBox(
          width: 10,
        ),
        CategoryProducts(
          press: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const FilmShowing())
            );
          },
          text: "Đang chiếu",
    ),
        SizedBox(
          width: 10,
        ),
        CategoryProducts(
          press: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const UpcomingFilm())
            );
          },
          text: "Sắp chiếu",
    ),
        SizedBox(
          width: 40,
        ),
        CategoryProducts(
          press: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const UpcomingFilm())
            );
          },
          text: ("Toàn Quốc"),
        ),
      ],
      )
    );

  }
}

class CategoryProducts extends StatelessWidget {

  const CategoryProducts ({
    Key? key,
    required this.text,
    required this.press,
  }): super (key: key);

  final String text;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(2.0),
      child: GestureDetector(
        onTap: press,
        child: Container(
            child: Chip(
              backgroundColor: Color(0xF2D59CDC),
                label: Row(
                  children: [
                    Text(text, style: TextStyle(color: Colors.white),),

                  ],
                )
            )
        ),
      ),
    );

  }

}

