import 'package:flutter/material.dart';
import '../FilmShowing/ListFilmShowing.dart';

class Film2 extends StatelessWidget {
  final filmList2 = [
    {
      'name': 'Kẻ Thứ Ba',
      'image': 'lib/img/Film9.jpg',
      'rate': '6.4',
      'classification': 'C18'
    },
    {
      'name': '578: Phát Đạn Của Kẻ Điên',
      'image': 'lib/img/Film10.jpg',
      'rate': '8.8',
      'classification': 'C18'
    },
    {
      'name': 'Phi Công Siêu Đẳng Maverick',
      'image': 'lib/img/Film11.jpg',
      'rate': '10.0',
      'classification': 'C13'
    },
    {
      'name': 'Thế Giới Khủng Long: Lãnh Địa',
      'image': 'lib/img/Film12.jpg',
      'rate': '9.7',
      'classification': 'C13'
    },
    {
      'name': 'Em Và Trịnh',
      'image': 'lib/img/Film13.jpeg',
      'rate': '7.8',
      'classification': 'C13'
    },
    {
      'name': 'Điện Thoại Đen',
      'image': 'lib/img/Film14.jpg',
      'rate': '5.5',
      'classification': 'C18'
    },
    {
      'name': 'Minions: Sự Trỗi Dậy Của Gru',
      'image': 'lib/img/Film15.jpg',
      'rate': '8.8',
      'classification': 'P'
    },
    {
      'name': 'Thor: Tình Yêu Và Sấm Sét',
      'image': 'lib/img/Film16.jpg',
      'rate': '9.3',
      'classification': 'C13'
    },

  ];

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: filmList2.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 0.50),
        itemBuilder: (BuildContext context, int index){
          return FilmState(
            name: filmList2[index]['name'],
            image: filmList2[index]['image'],
            rate: filmList2[index]['rate'],
            classification: filmList2[index]['classification'],
          );
        }
    );
  }
}



