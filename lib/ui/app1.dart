import 'package:flutter/material.dart';
import './loginscreen.dart';


class MyGalaxyCinema extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      home: LoginScreen(),
    );
  }

}
