import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../ui/category.dart';
import '../ui/FilmShowing/ListFilmShowing2.dart';
import '../ui/Cinema/cinemascreen.dart';
import './Proflie/profilescreen.dart';
import './SliverScreen/SliverScreen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}
class HomeScreenState extends State<StatefulWidget> {

  final List<String> imageList1 = [
    'https://www.galaxycine.vn/media/2022/4/27/promotion-hoadon100k-digital-1135x660_1651047359721.jpg',
    'https://www.galaxycine.vn/media/2022/4/27/qd_1651047852719.jpg',
    'https://www.galaxycine.vn/media/2022/4/18/nta-t5-2022-1135x660_1650254148097.jpg',
    'https://www.galaxycine.vn/media/2022/5/13/2048x682_1652415387211.jpg',
    'https://www.galaxycine.vn/media/2022/4/25/nsd-main-2048x682-sneak_1650882786339.jpg',
    'https://www.galaxycine.vn/media/2022/3/22/1135x660-1_1647919982925.jpg'

  ];

  final List<String> imageList2 = [
    'https://www.galaxycine.vn/media/2022/4/27/promotion-hoadon100k-digital-1135x660_1651047359721.jpg',
    'https://www.galaxycine.vn/media/2021/12/10/mbf-1221-1350x900---copy_1639131113561.jpg',
    'https://www.galaxycine.vn/media/2022/4/18/nta-t5-2022-1135x660_1650254148097.jpg',
    'https://www.galaxycine.vn/media/2021/1/18/1035x660_1610956392715.png',
    'https://www.galaxycine.vn/media/2022/3/22/1135x660-1_1647919982925.jpg'

  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            toolbarHeight: 45,
            title: Align(
              child: Text("MY GALAXY APP"),
              alignment: Alignment.center,
            ),
            backgroundColor: Color(0xF2D59CDC),
        ),
        body: Home(),

        bottomNavigationBar: Container(
          //padding: EdgeInsets.symmetric(vertical: 14),
          height: 65,
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color:  Color(0xF2D59CDC),
                blurRadius: 10
              )
            ],
           ),
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => const HomeScreen()),
                          );
                        },
                        icon: Icon(Icons.home_outlined)),
                    Text('Trang Chủ')
                  ],
                ),
                Column(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => const CinemaScreen()),
                          );
                        },
                        icon: Icon(Icons.camera_roll_outlined)),
                    Text('Rạp Phim')
                  ],
                ),
                Column(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => const SliverScreen()),
                          );
                        },
                        icon: Icon(Icons.camera_alt_outlined)),
                    Text('Điện Ảnh')
                  ],
                ),
                Column(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => const ProfileScreen()),
                          );
                        },
                        icon: Icon(Icons.person_outlined)),
                    Text('Tài Khoản')
                  ],
                )
              ],
            ),
          ),
        ),
    );

  }

  Widget Home() {
    return ListView(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      children: [
        Column(
          children: [
            SizedBox(
              height: 30,
            ),
            SaleSlide(),
            Category(),
            Container(
              height: 340,
              child: Film2(),
            ),
            SizedBox(
              height: 40,
            ),
            SaleSlide2(),
            SizedBox(
              height: 40,
            ),
          ],
        )
      ],

    );
  }


  Widget SaleSlide() {
    return Container(
      child: CarouselSlider(
        options: CarouselOptions(
            height: 160,
            enlargeCenterPage: true,
            autoPlay: true,
            aspectRatio: 16 / 9,
            autoPlayCurve: Curves.fastOutSlowIn,
            enableInfiniteScroll: true,
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            viewportFraction: 0.8
        ),
        items: imageList1.map((e) => ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Stack(
            fit: StackFit.expand,

            children: <Widget>[
              Image.network(e,
                fit: BoxFit.cover,
           )
            ],
          ) ,
        )).toList(),

      ),
    );
  }

  Widget SaleSlide2() {
    return Container(
      child: CarouselSlider(
        options: CarouselOptions(
            height: 160,
            enlargeCenterPage: true,
            autoPlay: true,
            aspectRatio: 16 / 9,
            autoPlayCurve: Curves.fastOutSlowIn,
            enableInfiniteScroll: true,
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            viewportFraction: 0.8
        ),
        items: imageList2.map((e) => ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Stack(
            fit: StackFit.expand,

            children: <Widget>[
              Image.network(e,
                fit: BoxFit.cover,
              )
              
              
            ],
          ) ,
        )).toList(),

      ),
    );
  }

}

