import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../FilmShowing/ListFilmShowing.dart';
import '../Cinema/cinemascreen.dart';
import '../homescreen.dart';
import '../Proflie/profilescreen.dart';
import '../SliverScreen/SliverScreen.dart';

class FilmShowing extends StatefulWidget {
  const FilmShowing({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return FilmShowingState();
  }
}
class FilmShowingState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 45,
        title: Align(
          child: Text("PHIM ĐANG CHIẾU"),
          alignment: Alignment.center,
        ),
        backgroundColor: Color(0xF2D59CDC),
      ),
      body: Home(),

      bottomNavigationBar: Container(
        //padding: EdgeInsets.symmetric(vertical: 14),
        height: 65,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color:  Color(0xF2D59CDC),
                blurRadius: 10
            )
          ],
        ),
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const HomeScreen()),
                        );
                      },
                      icon: Icon(Icons.home_outlined)),
                  Text('Trang Chủ')
                ],
              ),
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const CinemaScreen()),
                        );
                      },
                      icon: Icon(Icons.camera_roll_outlined)),
                  Text('Rạp Phim')
                ],
              ),
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const SliverScreen()),
                        );
                      },
                      icon: Icon(Icons.camera_alt_outlined)),
                  Text('Điện Ảnh')
                ],
              ),
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const ProfileScreen()),
                        );
                      },
                      icon: Icon(Icons.person_outlined)),
                  Text('Tài Khoản')
                ],
              )
            ],
          ),
        ),
      ),
    );

  }

  Widget Home() {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),

          Container(
            height: 550,
            child: Film(),
          )
        ],
      ),
    );
  }

}

