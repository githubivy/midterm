import 'package:flutter/material.dart';
import './ListFilmShowing.dart';

class Film2 extends StatelessWidget {
  final filmList = [
    {
      'name': 'Doctor Strange - Phù Thủy Tối Thượng Trong Đa Vũ Trụ Hỗn Loạn',
      'image': 'lib/img/Film1.jpeg',
      'rate': '9.5',
      'classification': 'C13'
    },
    {
      'name': 'Nghề Siêu Dễ',
      'image': 'lib/img/Film2.jpg',
      'rate': '7.8',
      'classification': 'C16'
    },
    {
      'name': 'Ký Sinh Trùng',
      'image': 'lib/img/Film3.jpg',
      'rate': '9.6',
      'classification': 'C18'
    },
    {
      'name': 'Ngôi Đền Kì Quái 3',
      'image': 'lib/img/Film4.jpg',
      'rate': '8.7',
      'classification': 'C16'
    },
    {
      'name': 'Sonic 2',
      'image': 'lib/img/Film5.jpg',
      'rate': '9.0',
      'classification': 'P'
    },
    {
      'name': 'Án Mạng Trên Sông Nile',
      'image': 'lib/img/Film6.jpg',
      'rate': '8.9',
      'classification': 'C18'
    },
    {
      'name': 'Thỏ Gà Rà Châu Báu',
      'image': 'lib/img/Film7.jpg',
      'rate': '8.8',
      'classification': 'P'
    },
    {
      'name': 'Những Bí Mật Của DumbleDore - Trở Lại Thế Giới Phép Thuật',
      'image': 'lib/img/Film8.jpg',
      'rate': '9.0',
      'classification': 'C13'
    },
    {
      'name': 'Coraline',
      'image': 'lib/img/img1.jpg',
      'rate': '9.2',
      'classification': 'C13'
    },
    {
      'name': 'Cô Dâu Ma',
      'image': 'lib/img/img2.jpg',
      'rate': '8.9',
      'classification': 'C13'
    },
  ];


  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: filmList.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 0.50),
        itemBuilder: (BuildContext context, int index){
          return FilmState(
            name: filmList[index]['name'],
            image: filmList[index]['image'],
            rate: filmList[index]['rate'],
            classification: filmList[index]['classification'],
          );
        }
    );
  }
}

