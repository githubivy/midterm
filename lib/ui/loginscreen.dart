import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:core';
import '../ui/homescreen.dart';
import './RegisterScreen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}
class LoginScreenState extends State<StatefulWidget> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();

  final _auth = FirebaseAuth.instance;

  String errorMessage = '';

  @override
  Widget build(BuildContext context) {
    final emailField = TextFormField(
      autofocus: false,
      controller: emailController,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.person,color: Color(0xF2D59CDC)),
        labelText: 'Email address',
      ),

      onSaved: (value)
      {
        emailController.text = value!;
      },
      textInputAction: TextInputAction.next,

    );

    final passwordField = TextFormField(
      autofocus: false,
      obscureText: true,
      controller: passwordController,
      decoration: InputDecoration(
          icon: Icon(Icons.password,color: Color(0xF2D59CDC),),
          labelText: 'Password',

      ),
      onSaved: (value)
      {
        passwordController.text = value!;
      },
      textInputAction: TextInputAction.done,
    );

    final loginButton = TextButton(
                  onPressed: (){
                    signIn(emailController.text, passwordController.text);
                  },
                  child: Text('Log In',style:
                  TextStyle(color: Colors.white,fontSize: 14),));
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 45,
        title: Align(
          child: Text("MY GALAXY APP"),
          alignment: Alignment.center,
        ),
        backgroundColor: Color(0xF2D59CDC),
      ),
      body: Center(
        child: Container(
          child: Form(
            key: _formKey,
            child: Column(

              children: [
                Column(
                  children: [
                    Image.asset('lib/img/welcome3.jpg',
                      width: 150,),
                    Text('Đăng Nhập Với Tài Khoản Của Bạn',
                      style: TextStyle( color: Color(0xF2D59CDC)),),
                  ],
                ),
                Padding(
                    padding: EdgeInsets.fromLTRB(40, 5, 40, 10),
                    child: emailField
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(40, 5, 40, 10),
                    child: passwordField
                ),
                SizedBox(
                  height: 10,
                ),
                Container(

                  height: 35,
                  width: 70,
                  decoration: BoxDecoration(
                    color: Color(0xF2D59CDC),
                    borderRadius: BorderRadius.circular(5),

                  ),
                  child:

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      loginButton
                    ],
                  ),
                ),
                Container(
                child: TextButton(
                    onPressed: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const SignInScreen()),
                      );
                    },
                    child: Text('Chưa có tài khoản đăng nhập? ',
                        style:
                        TextStyle(color: Color(0xF2D59CDC),fontSize: 14)
                    )
                ),
              ),
              ],
            ),
          ),
        ),
      )

    );
  }

void signIn(String email, String password) async {
    if(_formKey.currentState!.validate()) {
      await _auth
          .signInWithEmailAndPassword(email: email, password: password)
          .then((uid) => {
            Fluttertoast.showToast(msg: "Đặng Nhập Thành Công ^^ "),
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => HomeScreen())),

      }).catchError((e) {
        Fluttertoast.showToast(msg: e!.message);
      });
    }

}
}



