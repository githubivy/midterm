import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:galaxy/ui/loginscreen.dart';


class Contact extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child:
      Column(
        children: [
          Column(
            children: [
              Hotline(),
              Email(),
              CompanyInfo(),
              Clause(),
              PaymentPolicy(),
              SecurityPolicy(),
              FAQ(),
            ],
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                    onPressed: (){
                      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => LoginScreen()));
                    },
                    child: Text('Đăng Xuất',
                        style:
                        TextStyle(color: Colors.orange,fontSize: 14)
                    )
                )
              ],
            ),

          )
        ],
      ),


    );
  }

  Widget Hotline () {
    return Container(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget> [
            Row(
              children: [
                Text('Gọi ĐƯỜNG DÂY NÓNG',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                ),
                TextButton(
                    onPressed: (){},
                    child: Text('19002224 ',
                        style:
                        TextStyle(color: Colors.indigo,fontSize: 14)
                    )
                ),
              ],
            ),
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.arrow_forward_ios, size: 18, color: Colors.indigo,)),

          ]
      ),
    );
  }

  Widget Email () {
    return Container(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget> [
            Row(
              children: [
                Text('Email:',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                ),
                TextButton(
                    onPressed: (){},
                    child: Text('suppots@galaxy.com.vn',
                        style:
                        TextStyle(color: Colors.indigo,fontSize: 14)
                    )
                ),
              ],
            ),
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.arrow_forward_ios, size: 18, color: Colors.indigo,)),

          ]
      ),
    );
  }

  Widget CompanyInfo () {
    return Container(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget> [
            Row(
              children: [
                Text('Thông Tin Công Ty',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                ),
              ],
            ),
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.arrow_forward_ios, size: 18, color: Colors.indigo,)),

          ]
      ),
    );
  }

  Widget Clause () {
    return Container(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget> [
            Row(
              children: [
                Text('Điều Khoản Sử Dụng',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                ),
              ],
            ),
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.arrow_forward_ios, size: 18, color: Colors.indigo,)),

          ]
      ),
    );
  }

  Widget PaymentPolicy () {
    return Container(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget> [
            Row(
              children: [
                Text('Chính Sách Thanh Toán',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                ),
              ],
            ),
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.arrow_forward_ios, size: 18, color: Colors.indigo,)),

          ]
      ),
    );
  }

  Widget SecurityPolicy () {
    return Container(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget> [
            Row(
              children: [
                Text('Chính Sách Bảo Mật',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                ),
              ],
            ),
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.arrow_forward_ios, size: 18, color: Colors.indigo,)),

          ]
      ),
    );
  }

  Widget FAQ () {
    return Container(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget> [
            Row(
              children: [
                Text('FAQ',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                ),
              ],
            ),
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.arrow_forward_ios, size: 18, color: Colors.indigo,)),

          ]
      ),
    );
  }



}