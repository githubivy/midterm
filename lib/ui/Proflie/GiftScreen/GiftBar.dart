import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'GiftScreen.dart';



class GiftScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(

      child: Row(
          children: <Widget> [
            Expanded(
              flex: 2,
              child: (
                Text('Kho quà tặng',style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)
              ),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                    onPressed: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Gift()),
                      );
                    },
                    child: Text('Xem thêm',
                        style:
                        TextStyle(color: Colors.orange,fontSize: 14)
                    )
                )

              ],

            )

          ]

      ),
    );
  }


}

