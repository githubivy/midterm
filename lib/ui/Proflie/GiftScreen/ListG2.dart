import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'ListGift.dart';

class ListGift2 extends StatelessWidget {
  final giftList = [
    {
      'name': 'Star Popcorn',
      'image': 'lib/img/gift1.jpg',
      'stars': '50 STARS',
    },
    {
      'name': 'Star Popcorn 1',
      'image': 'lib/img/gift2.jpg',
      'stars': '100 STARS',
    },
    {
      'name': 'Star Popcorn 2',
      'image': 'lib/img/gift3.jpg',
      'stars': '150 STARS',
    },
    {
      'name': 'Star Popcorn 3',
      'image': 'lib/img/gift4.jpg',
      'stars': '200 STARS',
    },

  ];


  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: giftList.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 0.70),
        itemBuilder: (BuildContext context, int index){
          return GiftState(
            name: giftList[index]['name'],
            image: giftList[index]['image'],
            stars: giftList[index]['stars'],
          );
        }
    );
  }
}



