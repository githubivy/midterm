import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:galaxy/icons/my_flutter_app_icons.dart';

class ListGift extends StatelessWidget {
  final giftList = [
    {
      'name': 'Star Popcorn',
      'image': 'lib/img/gift1.jpg',
      'stars': '50 STARS',
    },
    {
      'name': 'Star Popcorn 1',
      'image': 'lib/img/gift2.jpg',
      'stars': '100 STARS',
    },
    {
      'name': 'Star Popcorn 2',
      'image': 'lib/img/gift3.jpg',
      'stars': '150 STARS',
    },
    {
      'name': 'Star Popcorn 3',
      'image': 'lib/img/gift4.jpg',
      'stars': '200 STARS',
    },

  ];


  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: giftList.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 0.70),
        itemBuilder: (BuildContext context, int index){
          return GiftState(
            name: giftList[index]['name'],
            image: giftList[index]['image'],
            stars: giftList[index]['stars'],
          );
        }
    );
  }
}


class GiftState extends StatelessWidget {
  final name;
  final image;
  final stars;

  GiftState ({
    this.name,
    this.image,
    this.stars,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: Image.asset(image,width: 200,height: 100,),
        ),
        ListTile(
          title: Text(name, style: TextStyle(fontWeight: FontWeight.w600),),
          subtitle: Container(
            child: Column(
                children: [
                  Row(
                    children: [
                      SizedBox(
                        height: 30,
                      ),
                      Icon(MyFlutterApp.gift,size: 20, color: Colors.amber),
                      SizedBox(
                        width: 10,
                      ),
                      Text(stars)
                    ],
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Container(

                    height: 35,
                    width: 140,
                    decoration: BoxDecoration(
                      color: Colors.orange,
                      borderRadius: BorderRadius.circular(5),

                    ),
                    child:

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(

                            onPressed: (){},
                            child: Text('Nhận Ngay',style: TextStyle(color: Colors.white, fontSize: 14),))
                      ],
                    ),
                  )

                ]
            ),
          ),
        )
      ],
    );
  }

}


