import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:galaxy/icons/my_flutter_app_icons.dart';


class MenuBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              height: 40,
              decoration: BoxDecoration(
                border: Border(
                  right: BorderSide (
                    color: Colors.grey,
                    width: 1.0,
                  )
                )
              ),
              child:
              TextButton.icon(
                icon: Icon(MyFlutterApp.pencil, color: Colors.black,size: 20,),
                label:Text('Thông tin', textAlign: TextAlign.center, style:
                TextStyle(color: Colors.black,),
                ),
                onPressed: () {},

              ),
            )

          ),
          Expanded(
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                    border: Border(
                        right: BorderSide (
                          color: Colors.grey,
                          width: 1.0,
                        )
                    )
                ),
                child:
                TextButton.icon(
                  icon: Icon(MyFlutterApp.history, color: Colors.black,size: 20,),
                  label:Text('Giao Dịch', textAlign: TextAlign.center, style:
                  TextStyle(color: Colors.black,),
                  ),
                  onPressed: () {},

                ),
              )

          ),
          Expanded(
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                ),
                child:
                TextButton.icon(
                  icon: Icon(MyFlutterApp.gift, color: Colors.black,size: 20,),
                  label:Text('Quà Tặng', textAlign: TextAlign.center, style:
                  TextStyle(color: Colors.black,),
                  ),
                  onPressed: () {},

                ),
              )

          ),
        ],
      ),
    );
  }



}