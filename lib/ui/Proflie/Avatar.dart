import 'package:flutter/material.dart';

class Ava extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
         Column(
            children: [
              CircleAvatar(
                radius: 30,
                backgroundImage: AssetImage('lib/img/ava.jpg'),
              )
            ],
          ),
          SizedBox(
            width: 10,
          ),
          Info(),
          MemberCode ()
        ],
      ),
    );
  }

  Widget Info () {
    return Column(
      children: [
        Row(
          children: [
            Column(
              children: [
                CircleAvatar(
                  radius: 10,
                  backgroundImage: AssetImage('lib/img/ava2.jpg'),
                )
              ],
            ),
            Column(
              children: [
                SizedBox(
                  width: 5,
                )
              ],
            ),
            Column(
              children: [
                Text('DANG NHAT TUONG VY',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),)
              ],
            ),

          ],
        ),
        Row(

          children: [
            Column(
              children: [
                Text('X-Star',
                  style: TextStyle( fontSize: 10, color: Colors.grey),)
              ],
            ),
            Column(
              children: [
                SizedBox(
                  width: 110,
                )
              ],
            ),
          ],
        ),

        Row(
          children: [
            Column(
              children: [
                Icon(Icons.wallet_giftcard_rounded, color: Colors.orange, size: 17,)
              ],
            ),
            Column(
              children: [
                SizedBox(
                  width: 5,
                )
              ],
            ),
            Column(
              children: [
                Text('162 Stars',
                  style: TextStyle(fontSize: 14),)
              ],
            ),
            Column(
              children: [
                SizedBox(
                  width: 100,
                )
              ],
            ),
          ],
        )
      ],
    );
  }
  
  Widget MemberCode () {
    return Column(
      children: [
        Row(
          children: [
            IconButton(
              icon: Image.asset('lib/img/ava1.jpg'),
              iconSize: 40,
              onPressed: () {},
            )
          ],
        ),
        Row(
          children: [
            Text('Mã thành viên', style: TextStyle(color: Colors.orange, fontSize: 12, fontWeight: FontWeight.w600),)
          ],
        )

      ],
    );
  }

}
