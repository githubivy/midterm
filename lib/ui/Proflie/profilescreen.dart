import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:galaxy/icons/my_flutter_app_icons.dart';
import '../Cinema/cinemascreen.dart';
import '../homescreen.dart';
import './Avatar.dart';
import './MenuBar.dart';
import './Spending.dart';
import './RangeSlider.dart';
import 'GiftScreen/GiftBar.dart';
import 'GiftScreen/ListG2.dart';
import './Contact.dart';
import '../SliverScreen/SliverScreen.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ProfileScreenState();
  }
}
class ProfileScreenState extends State<StatefulWidget> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 45,
        actions: [
          IconButton(
            icon: const Icon(MyFlutterApp.notifications_none),
            onPressed: () {
              // handle the press
            },
          ),
          IconButton(
            icon: const Icon(Icons.settings_sharp),
            onPressed: () {
              // handle the press
            },
          ),
        ],
        title: Align(
          child: Text("Tài Khoản"),
          alignment: Alignment.center,
        ),
        backgroundColor: Color(0xF2D59CDC),
      ),
      body: Home(),

      bottomNavigationBar: Container(
        //padding: EdgeInsets.symmetric(vertical: 14),
        height: 65,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Color(0xF2D59CDC),
                blurRadius: 10
            )
          ],
        ),
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const HomeScreen()),
                        );
                      },
                      icon: Icon(Icons.home_outlined)),
                  Text('Trang Chủ')
                ],
              ),
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const CinemaScreen()),
                        );
                      },
                      icon: Icon(Icons.camera_roll_outlined)),
                  Text('Rạp Phim')
                ],
              ),
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const SliverScreen()),
                        );
                      },
                      icon: Icon(Icons.camera_alt_outlined)),
                  Text('Điện Ảnh')
                ],
              ),
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ProfileScreen()),
                        );
                      },
                      icon: Icon(Icons.person_outlined)),
                  Text('Tài Khoản')
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
  Widget Home (){
    return ListView(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      padding: EdgeInsets.all(10),
      children: [
        SizedBox(
          height: 20,
        ),
        Ava(),
        SizedBox(
          height: 20,
        ),
        MenuBar(),
        SizedBox(
          height: 20,
        ),

        Spending(),
        RSlider(),
        SizedBox(
          height: 20,
        ),
        GiftScreen(),

        Container(
          child: ListGift2(),
          height: 220,
        ),
        Contact(),


      ],

    );
  }
}
