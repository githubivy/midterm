import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:galaxy/icons/my_flutter_app_icons.dart';


class Spending extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget> [
          Column(
            children: [
              Text('Chi tiêu năm 2022',style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)
            ],
          ),
          Column(
            children: [
              IconButton(
                  onPressed: () {},
                  icon: Icon(MyFlutterApp.info_circled_alt, size: 18, color: Colors.indigo,)),
            ],
          ),
          Expanded(
              child:
              Text('1,535,500đ',textAlign: TextAlign.right, style: TextStyle(color: Colors.orange,fontSize: 16),)
          )

        ]

      ),

    );
  }

}