import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../homescreen.dart';
import '../Proflie/profilescreen.dart';
import '../Cinema/cinemascreen.dart';
import 'ListSliverScreen.dart';


class SliverScreen extends StatefulWidget {
  const SliverScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SliverScreenState();
  }
}
class SliverScreenState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 45,
        title: Align(
          child: Text("Điện Ảnh"),
          alignment: Alignment.center,
        ),
        backgroundColor: Color(0xF2D59CDC),

      ),
      body:

      Home(),

      bottomNavigationBar: Container(
        //padding: EdgeInsets.symmetric(vertical: 14),
        height: 65,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color:  Color(0xF2D59CDC),
                blurRadius: 10
            )
          ],
        ),
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const HomeScreen()),
                        );
                      },
                      icon: Icon(Icons.home_outlined)),
                  Text('Trang Chủ')
                ],
              ),
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const CinemaScreen()),
                        );
                      },
                      icon: Icon(Icons.camera_roll_outlined)),
                  Text('Rạp Phim')
                ],
              ),
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const SliverScreen()),
                        );

                      },
                      icon: Icon(Icons.camera_alt_outlined)),
                  Text('Điện Ảnh')
                ],
              ),
              Column(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const ProfileScreen()),
                        );
                      },
                      icon: Icon(Icons.person_outlined)),
                  Text('Tài Khoản')
                ],
              )
            ],
          ),
        ),
      ),
    );

  }

  Widget Home() {
    return ListView(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        children: [
          Column(
            children: [
              SearchField(),
              MenuGiftScreen(),
              Container(
                height: 500,
                child:
                ListSliverScreen()
                ,
              )
            ],
          )
        ],


    );
  }
  Widget SearchField() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Colors.grey.withOpacity(0.5),
          width: 1.0,
        ),
      ),
      margin: EdgeInsets.all(12),
      child: Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 8),
            child: Icon(
              Icons.search,
              color: Colors.grey,
              size: 20,
            ),
          ),
          Expanded(
            child: TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Tìm Kiếm",
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget MenuGiftScreen() {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
              child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      border: Border(
                          right: BorderSide (
                            color: Colors.grey,
                            width: 1.0,
                          ),
                          bottom: BorderSide (
                            color: Colors.grey,
                            width: 1.0,
                          )
                      )
                  ),
                  child:
                  TextButton(
                      onPressed: (){},
                      child: Text('Bình Luận',
                        style:
                        TextStyle(color: Colors.grey, fontSize: 14),
                      )
                  )
              )

          ),
          Expanded(
              child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      border: Border(
                          right: BorderSide (
                            color: Colors.grey,
                            width: 1.0,
                          ),
                          bottom: BorderSide (
                            color: Colors.grey,
                            width: 1.0,
                          )
                      )
                  ),
                  child:
                  TextButton(
                      onPressed: (){},
                      child: Text('Tin Tức',
                        style:
                        TextStyle(color: Colors.grey, fontSize: 14),
                      )
                  )
              )

          ),
          Expanded(
              child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      border: Border(

                          bottom: BorderSide (
                            color: Colors.grey,
                            width: 1.0,
                          )
                      )
                  ),
                  child:
                  TextButton(
                      onPressed: (){},
                      child: Text('Nhân Vật',
                        style:
                        TextStyle(color: Colors.grey, fontSize: 14),
                      )
                  )
              )

          ),
        ],
      ),
    );

  }
}

