import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListSliverScreen extends StatelessWidget {
  final SliverScreen = [
    {
      'name': '[Review] Doctor Strange 2: Strange Đối Đầu Kẻ Ác Mạnh Nhất MCU?',
      'image': 'lib/img/SliverScreen1.jpg',
    },
    {
      'name': '[Review] Nghề Siêu Dễ: Hài Hước Và Không Kém Phần Ý Nghĩa',
      'image': 'lib/img/SliverScreen2.jpg',
    },
    {
      'name': '[Review] Doctor Strange 2: X-Men Xuất Hiện, Wanda Sẽ "Ra Đi"',
      'image': 'lib/img/SliverScreen3.jpg',
    },
    {
      'name': '[Review] Đêm Tối Rực Rỡ: Khi Gia Đình Là Ngọn Nguồn Bất Hạnh!',
      'image': 'lib/img/SliverScreen4.jpg',
    },
    {
      'name': '[Review] MorBius: Có Xứng Tầm Kẻ Thù Của Người Nhện?',
      'image': 'lib/img/SliverScreen5.jpg',
    },
  ];


  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: SliverScreen.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 1, childAspectRatio: 0.95),
        itemBuilder: (BuildContext context, int index){
          return GiftState(
            name: SliverScreen[index]['name'],
            image: SliverScreen[index]['image'],
          );
        }
    );
  }
}


class GiftState extends StatelessWidget {
  final name;
  final image;


  GiftState ({
    this.name,
    this.image,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(

          child: Image.asset(image,width: 330,height: 280,),
        ),
        ListTile(
          title: Text(name, style: TextStyle(fontWeight: FontWeight.w600),),
          subtitle: Container(
            child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                          TextButton(
                                onPressed: (){},
                                child: Text('Xem Thêm',style: TextStyle(color: Color(0xF2D59CDC), fontSize: 14),))
                          ],

          ),
        )
        )
      ],
    );
  }

}


